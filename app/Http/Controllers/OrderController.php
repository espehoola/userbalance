<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Requests\Api\OrderStore;
use App\Models\Order;
use App\Models\User;
use DB;
use Log;

/**
 * Class OrderController
 *
 * @author Dmitriy Bakhtin <dbahtin@yandex.ru>
 */
class OrderController extends Controller
{
    /**
     * @param OrderStore $request
     * @return array
     * @throws \Exception
     */
    public function store(OrderStore $request): array
    {
        Log::debug('Create order', $request->all());
        DB::beginTransaction();
        try {
            $order = new Order();
            $order->user_id = \Auth::user()->id;
            $order->charge = $request->get('charge');
            $order->save();
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw new \Exception($exception->getMessage());
        }

        return [
            'status' => 'success',
            'message' => [
                'newOrderId' => $order->id,
            ],
        ];
    }
}
