<?php
declare(strict_types = 1);

use Illuminate\Database\Seeder;

use App\Models\User;

/**
 * Class DatabaseSeeder
 * @author Dmitriy Bakhtin <dbahtin@yandex.ru>
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->create();
    }
}
